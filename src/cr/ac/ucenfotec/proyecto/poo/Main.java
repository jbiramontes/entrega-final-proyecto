
package cr.ac.ucenfotec.proyecto.poo;


import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.apache.commons.mail.EmailException;

import java.sql.Connection;
import java.sql.SQLException;

/**
 *
 * Esta es la clase principal para lanzar la aplicacion
 * @author: Jorge Biramontes Calvo
 * @version: 1.45
 */

public class Main extends Application {
    static Connection cnx;

    @Override
    public void start(Stage primaryStage) throws Exception {

        Parent root = FXMLLoader.load(getClass().getResource("iufx/InicioSesion.fxml"));
        Stage ventana = primaryStage;
        ventana.setTitle("El Baul de los recuerdos");
        ventana.setScene(new Scene(root, 606, 377));
        ventana.show();
    }

    public static void main(String[] args) throws SQLException, EmailException {
        launch(args);

    }


}


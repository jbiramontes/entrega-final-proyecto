package cr.ac.ucenfotec.proyecto.poo.controlador;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;

import javax.swing.*;
import java.net.URL;
import java.util.ResourceBundle;

public class ControllerRegistroCancionAdmin implements Initializable {
    Controlador controlador = new Controlador();
    ToScene toScene=new ToScene();
    @FXML
    private Button btnRegistrar;

    @FXML
    private Button atras;

    @FXML
    private TextField nombreCancion;

    @FXML
    private TextField nombreArtista;

    @FXML
    private TextField calificacion;

    @FXML
    private TextField nombreCompositor;

    @FXML
    private TextField nombreGenero;

    @FXML
    private ComboBox SeleccionGenero;
    @FXML
    private ComboBox SeleccionArtista;
    @FXML
    private ComboBox SeleccionCompositor;

    @FXML
    void SelectGenero(ActionEvent event) {


    }

    @FXML
    void retroceder(ActionEvent event) {
        toScene.toScene("MenuUsuarioAdmin.fxml", event);
    }

    @FXML
    void registroCancion(ActionEvent event) {
        String genero = SeleccionGenero.getSelectionModel().getSelectedItem().toString();
        String artista = SeleccionArtista.getSelectionModel().getSelectedItem().toString();
        String compositor = SeleccionCompositor.getSelectionModel().getSelectedItem().toString();
        int calificacionNumero = Integer.parseInt(calificacion.getText());

        if (calificacionNumero < 0 || calificacionNumero > 5) {
            JOptionPane.showMessageDialog(null, "La calificacion es de 0 a 5 solamente");
        } else {

            controlador.registrarCancion(nombreCancion.getText(), calificacionNumero, artista, genero, compositor);
        }
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        for (int i = 0; i < controlador.listarGenerosMenu().length; i++) {
            SeleccionGenero.getItems().addAll(controlador.listarGenerosMenu()[i]);
        }
        for (int i = 0; i < controlador.listarNombreArtistas().length; i++) {
            SeleccionArtista.getItems().addAll(controlador.listarNombreArtistas()[i]);
        }
        for (int i = 0; i < controlador.listarNombreCompositores().length; i++) {
            SeleccionCompositor.getItems().addAll(controlador.listarNombreCompositores()[i]);
        }
    }
}

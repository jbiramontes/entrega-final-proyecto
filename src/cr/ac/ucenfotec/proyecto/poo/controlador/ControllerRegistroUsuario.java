package cr.ac.ucenfotec.proyecto.poo.controlador;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

import javax.swing.*;
import java.net.URL;
import java.util.ResourceBundle;

public class ControllerRegistroUsuario implements Initializable {

    Controlador controlador = new Controlador();
    @FXML
    private TextField fechaNacimiento;

    @FXML
    private TextField edad;

    @FXML
    private TextField nombre;

    @FXML
    private TextField apellidoUno;

    @FXML
    private TextField apellidoDos;

    @FXML
    private Button btnRegistrar;

    @FXML
    private Button atras;

    @FXML
    private TextField nombreUsuario;

    @FXML
    private TextField correo;

    @FXML
    private TextField id;

    @FXML
    private TextField avatar;

    @FXML
    private ComboBox SeleccionPais;

    @FXML
    private TextField tipoUsuario;

    @FXML
    private PasswordField password;

    @FXML
    void registroUsuario(ActionEvent event) {

if(nombre.getText().length()!=0||apellidoUno.getText().length()!=0||fechaNacimiento.getText().length()!=0||SeleccionPais.getSelectionModel().getSelectedItem().toString().length()!=0||id.getText().length()!=0
        ||correo.getText().length()!=0||nombreUsuario.getText().length()!=0||password.getText().length()!=0) {
    int tipoUser=Integer.parseInt(tipoUsuario.getText());
    controlador.registrarUsuario(nombre.getText(), apellidoUno.getText(), apellidoDos.getText(),
            fechaNacimiento.getText(), SeleccionPais.getSelectionModel().getSelectedItem().toString(), id.getText(), avatar.getText(), correo.getText(), nombreUsuario.getText(), password.getText(), tipoUser);
    //new ToScene().toScene("InicioSesion.fxml", event);
}else{    JOptionPane.showMessageDialog(null,"Datos incompletos");

}
}

    @FXML
    void retroceder(ActionEvent event) {


        new ToScene().toScene("InicioSesion.fxml", event);

    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        for (int i = 0; i < controlador.listarPaises().length; i++) {
            SeleccionPais.getItems().addAll(controlador.listarPaises()[i]);
        }
    }
}

package cr.ac.ucenfotec.proyecto.poo.controlador;

import cr.ac.ucenfotec.proyecto.poo.bl.entidades.*;
import cr.ac.ucenfotec.proyecto.poo.bl.logica.Gestor;
import cr.ac.ucenfotec.proyecto.poo.iu.IU;
import javafx.event.ActionEvent;
import org.apache.commons.mail.EmailException;

import javax.swing.*;
import java.awt.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Controlador {
    private IU Interfaz = new IU();
    private Gestor gestor = new Gestor();

    public void ejecutarPrograma() throws EmailException {
        int opcion = 1;
        Interfaz.menuPrincipal();
        opcion = Interfaz.leerOpcionMenu();
        //   switchMenuPrincipal(opcion);
        if (opcion == 0) {
            gestor.salirPrograma();
        }
    }


    public void listar(int opcion, String nombre) {
        if (opcion == 1) {
            List<ListaReproduccion> lista = gestor.mostarListasreproduccion();
            for (int i = 0; i < lista.size(); i++) {
                System.out.println(lista.get(i).getNombreLista() + ", " + lista.get(i).getFechaCreacion() + ", " + lista.get(i).getNombreUsuario() + ", ");
                for (int j = 0; j < lista.get(i).getCancionesLista().size(); j++) {
                    ArrayList<Cancion> canciones = new ArrayList<>();
                    canciones.add(lista.get(i).getCancionesLista().get(j));
                    ArrayList<String> nombres = new ArrayList<>();
                    nombres.add(canciones.get(0).getNombreCancion());
                    System.out.println(nombres.get(0));
                }

            }
        }

        if (opcion == 2) {
            List<Artista> lista = gestor.listarArtistas();
            for (int i = 0; i < lista.size(); i++) {
                System.out.println(lista.get(i));

            }
        }
        if (opcion == 3) {
            List<Compositor> lista = gestor.listarCompositores();
            for (int i = 0; i < lista.size(); i++) {
                System.out.println(lista.get(i).getNombre() + ", " + lista.get(i).getApellidoUno() + ", " + lista.get(i).getPais() + ", " + lista.get(i).getEdad());
            }
        }
        if (opcion == 4) {
            List<Cancion> lista = gestor.listarCanciones();
            for (int i = 0; i < lista.size(); i++) {
                System.out.println(lista.get(i));
            }
        }
        if (opcion == 5) {
            List<Catalogo> lista = gestor.listarCatalogo();
            for (int i = 0; i < lista.size(); i++) {
                if (nombre.contentEquals(lista.get(i).getUsuarioCatalogo().getNombreUsuario())) {
                    System.out.print("Usuario " + lista.get(i).getUsuarioCatalogo().getNombreUsuario());
                    for (int j = 0; j < lista.get(i).getCancionesLista().size(); j++) {
                        System.out.println(" " + "Cancion " + lista.get(i).getCancionesLista().get(j).getNombreCancion());
                    }

                }
            }
        }
        if (opcion == 9) {
            List<Album> lista = gestor.listarAlbum();
            for (int i = 0; i < lista.size(); i++) {
                System.out.println(lista.get(i).getNombreAlbum() + " " + lista.get(i).getFechaLanzamiento());


            }
        }


        if (opcion == 10) {

            List<Album> lista = gestor.listarAlbumesNombre(nombre);
            for (int i = 0; i < lista.size(); i++) {
                System.out.println(lista.get(i).getNombreAlbum() + lista.get(i).getCancionesAlbum());
            }
        }
    }

    public String[] listarGenerosMenu() {
        int i = 0;
        List<Genero> lista = gestor.listarGeneros();
        String[] listaGeneros = new String[lista.size()];

        for (Genero unGenero : lista) {
            listaGeneros[i] = unGenero.getNombre();
            i++;


        }
        return listaGeneros;
    }

    public String[] listarNombreArtistas() {
        int i = 0;
        List<Artista> lista = gestor.listarArtistas();
        String[] listaArtistas = new String[lista.size()];

        for (Artista unArtista : lista) {
            listaArtistas[i] = unArtista.getNombreArtistico();
            i++;
        }
        return listaArtistas;
    }

    public String[] listarNombreDeListas() {
        int i = 0;
        List<ListaReproduccion> lista = gestor.mostarListasreproduccion();
        String[] listasReproduccion = new String[lista.size()];

        for (ListaReproduccion unaLista : lista) {
            listasReproduccion[i] = unaLista.getNombreLista();
            i++;
        }
        return listasReproduccion;
    }

    public String[] listarNombreDeUsuarios() {
        int i = 0;
        List<Usuario> lista = gestor.listarUsuarios();
        String[] listausuarios = new String[lista.size()];

        for (Usuario unUsuario : lista) {
            listausuarios[i] = unUsuario.getNombreUsuario();
            i++;
        }
        return listausuarios;
    }

    public String[] listarNombreCompositores() {
        int i = 0;
        List<Compositor> lista = gestor.listarCompositores();
        String[] listaCompositores = new String[lista.size()];

        for (Compositor compositor : lista) {
            listaCompositores[i] = compositor.getNombre();
            i++;
        }
        return listaCompositores;
    }

    public String[] listarNombreCanciones() {
        int i = 0;
        List<Cancion> lista = gestor.listarCanciones();
        String[] listaCanciones = new String[lista.size()];

        for (Cancion unaCancion : lista) {
            listaCanciones[i] = unaCancion.getNombreCancion();
            i++;


        }
        return listaCanciones;
    }

    public String[] listarPaises() {
        int i = 0;
        List<String> lista = gestor.listarPais();
        String[] listaPaises = new String[lista.size()];

        for (String pais : lista) {
            listaPaises[i] = pais;
            i++;


        }
        return listaPaises;
    }

    public void crearAlbum(String nombreAlbum, String fecha) {
        try {

            boolean respuesta = gestor.agregarAlbum(nombreAlbum, fecha, null,null);
            if (respuesta) {
                JOptionPane.showMessageDialog(null, "Album registrado");
            } else {
                JOptionPane.showMessageDialog(null, "Album no se pudo registrar intente nuevamente");

            }
        } catch (HeadlessException e) {

            e.printStackTrace();
        }
    }

    public void crearListaReproduccion(String nombreLista, String nombreUSuario) {
        try {
            if (nombreLista.length() == 0 || nombreUSuario.length() == 0) {
                JOptionPane.showMessageDialog(null, "No puede dejar espacios vacios");
            } else {
                LocalDate fechaCreacion = LocalDate.now();

                gestor.agregarListaReproduccion(nombreLista, fechaCreacion, null, nombreUSuario);
                JOptionPane.showMessageDialog(null, "Lista registrada con exito favor introduzca las canciones");

            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al registrar la lista");

        }

    }

    public void agregarCancionColeccion(String nombreUsuario, String nombreCancion) {
        int buscar = 0;

        try {
            String nombres[] = listarNombreDeUsuarios();
            for (int i = 0; i < nombres.length; i++) {
                if (nombreUsuario.contentEquals(nombres[i])) {
                    buscar++;
                }
            }
            if (nombreUsuario.length() == 0 || nombreCancion.length() == 0) {
                JOptionPane.showMessageDialog(null, "No puede dejar espacios vacios");
            }
            if (buscar == 0) {
                JOptionPane.showMessageDialog(null, "Nombre " + nombreUsuario + " no existe");

            } else {
                Usuario nuevoUsuario = new Usuario();
                nuevoUsuario.setNombreUsuario(nombreUsuario);
                Cancion nuevaCancion = new Cancion();
                nuevaCancion.setNombreCancion(nombreCancion);


                gestor.agregarCancionesCatalogo(nuevaCancion, nuevoUsuario);
                JOptionPane.showMessageDialog(null, "Cancion registrada");
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al registrar ");
        }
    }

    public void agregarCancionLista(String nombreLista, String nombreCancion) {
        int buscar = 0;

        try {
            String nombres[] = listarNombreDeListas();
            for (int i = 0; i < nombres.length; i++) {
                if (nombreLista.contentEquals(nombres[i])) {
                    buscar++;
                }
            }
            if (nombreLista.length() == 0 || nombreCancion.length() == 0) {
                JOptionPane.showMessageDialog(null, "No puede dejar espacios vacios");
            }
            if (buscar == 0) {
                JOptionPane.showMessageDialog(null, "Nombre " + nombreLista + " no existe");

            } else {
                gestor.agregarCancionesLista(nombreLista, nombreCancion);
                JOptionPane.showMessageDialog(null, "Cancion registrada");
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al registrar la lista");
        }
    }

    public void registrarCancion(String nombre, int calificacion, String artista, String genero, String compositor) {

        gestor.agregarCancion(nombre, calificacion, artista, genero, compositor);
        JOptionPane.showMessageDialog(null, "Registrado con éxito");
    }

    public void registrarArtista(String nombre, String apellidoUno, String apellidoDos, String fechaNacimiento, String pais, String descripcion, String nombreArtistico, String fechaDefuncion) {

        try {

            if (gestor.agregarArtista(nombre, apellidoUno, apellidoDos, fechaNacimiento, pais, descripcion, nombreArtistico, fechaDefuncion)) {
                JOptionPane.showMessageDialog(null, "Artista registrado correctamente");
            } else {
                JOptionPane.showMessageDialog(null, "El Artista no pudo ser registrado");
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "El Artista no pudo ser registrado");

        }

    }

    public void registrarCompositor(String nombre, String apellidoUno, String apellidoDos, String fechaNacimiento, String pais) {

        try {

            if (gestor.agregarCompositor(nombre, apellidoUno, apellidoDos, fechaNacimiento, pais)) {
                JOptionPane.showMessageDialog(null, "Compositor registrado correctamente");
            } else {
                JOptionPane.showMessageDialog(null, "El Compositor no pudo ser registrado");
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "El Compositor no pudo ser registrado");

        }

    }

    public void Salir() {
        JOptionPane.showMessageDialog(null, "Gracias por utilizar nuestra plataforma ");
        gestor.salirPrograma();
    }

    public int inicioSesion(String usuario, String contrasena) {
        int tipoUsuario = gestor.comprobarCredenciales(usuario, contrasena);
        try {
            String opcionNula = "";
            if (contrasena.equals(opcionNula) && usuario.equals(opcionNula)) {
                //JOptionPane.showMessageDialog(null, "No ingreso el usuario o la contrasena intente de nuevo");
                return tipoUsuario = 10;
            }
            if (tipoUsuario == 10) {
                //System.out.println("Credenciales no validas");
                return tipoUsuario;
            } else {
                //System.out.println("Bienvenido a la aplicacion");
                return tipoUsuario;
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Usted ha ingresado datos no validos");
        }
        return tipoUsuario = 10;
    }

    public void registrarUsuario(String nombre, String apellidoUno, String apellidoDos, String fechaNacimiento, String pais,
                                 String identificacion, String imagen, String correoUsuario, String nombreUsuario, String contrasena, int tipoUsuario) {
        try {

            boolean respuesta = gestor.agregarUsuario(nombre, apellidoUno, apellidoDos, fechaNacimiento, pais, identificacion, imagen, correoUsuario, nombreUsuario, contrasena, tipoUsuario);
            if (respuesta) {
                JOptionPane.showMessageDialog(null, "Usuario registrado con exito");
                JOptionPane.showMessageDialog(null, "Le hemos enviado un codigo de acceso a su correo");


            } else {
                JOptionPane.showMessageDialog(null, "Favor revise edad de usuario o tipo");

            }
        } catch (HeadlessException e) {
            JOptionPane.showMessageDialog(null, "Usuario no se pudo registrar");
            e.printStackTrace();
        }
    }

    public void registrargenero(String nombre, String descripcionombreUsuario) {
        try {

            boolean respuesta = gestor.agregarGenero(nombre, descripcionombreUsuario);
            if (respuesta) {
                JOptionPane.showMessageDialog(null, "Genero registrado");
            } else {
                JOptionPane.showMessageDialog(null, "Genero no se pudo registrar intente nuevamente");

            }
        } catch (HeadlessException e) {

            e.printStackTrace();
        }
    }

    public void modificargenero(String nombreGenero, String nuevoNombre, String descripcionombreUsuario) {
        try {

            boolean respuesta = gestor.modificarGenero(nombreGenero, nuevoNombre, descripcionombreUsuario);
            if (respuesta) {
                JOptionPane.showMessageDialog(null, "Genero modificado");
            } else {
                JOptionPane.showMessageDialog(null, "Genero no se pudo modificar");

            }
        } catch (HeadlessException e) {

            e.printStackTrace();
        }
    }

    public void eliminargenero(String nombre, String descripcionombreUsuario) {
        try {

            boolean respuesta = gestor.eliminarGenero(nombre, descripcionombreUsuario);
            if (respuesta) {
                JOptionPane.showMessageDialog(null, "Genero eliminado");
            } else {
                JOptionPane.showMessageDialog(null, "Genero no se pudo eliminar");

            }
        } catch (HeadlessException e) {

            e.printStackTrace();
        }
    }
    public void eliminarAlbum(String nombre) {
        try {

            boolean respuesta = gestor.eliminarAlbum(nombre);
            if (respuesta) {
                JOptionPane.showMessageDialog(null, "Album eliminado");
            } else {
                JOptionPane.showMessageDialog(null, "Album no se pudo eliminar");

            }
        } catch (HeadlessException e) {

            e.printStackTrace();
        }
    }
}

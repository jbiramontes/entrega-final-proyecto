package cr.ac.ucenfotec.proyecto.poo.controlador;

import javafx.event.ActionEvent;
import javafx.fxml.Initializable;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class ControllerListaReproduccion implements Initializable {
    Controlador controlador = new Controlador();
    ToScene toScene = new ToScene();
    @FXML
    private Label mensajeBienvenida;

    @FXML
    private Button btnCrearListaReproduccion;

    @FXML
    private Button btnAgregarCancion;

    @FXML
    private Button atras;

    @FXML
    private Button btnCrearAlbum;

    @FXML
    private Button btnMostarListas;

    @FXML
    private TextField nombreListaRegistro;

    @FXML
    private TextField nombreUsuario;

    @FXML
    private ComboBox listaCanciones;


    @FXML
    void crearLista(ActionEvent event) {
        controlador.crearListaReproduccion(nombreListaRegistro.getText(), nombreUsuario.getText());
    }

    @FXML
    void mostrarlistasReproduccion(ActionEvent event) {
        controlador.listar(1, null);
    }

    @FXML
    void Atras(ActionEvent event) {
        toScene.toScene("MenuUsuario.fxml", event);
    }

    @FXML
    void agregarCancion(ActionEvent event) {
        controlador.agregarCancionLista(nombreListaRegistro.getText(), listaCanciones.getSelectionModel().getSelectedItem().toString());

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        for (int i = 0; i < controlador.listarNombreCanciones().length; i++) {
            listaCanciones.getItems().addAll(controlador.listarNombreCanciones()[i]);
        }
    }
}

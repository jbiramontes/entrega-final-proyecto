package cr.ac.ucenfotec.proyecto.poo.controlador;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;

import javax.swing.*;
import java.time.format.DateTimeFormatter;


public class ControllerRegistroArtista {

    Controlador controlador = new Controlador();
    ToScene toScene = new ToScene();
    @FXML
    private DatePicker fechaNacimiento;

    @FXML
    private TextField edad;

    @FXML
    private TextField nombre;

    @FXML
    private TextField apellidoUno;

    @FXML
    private TextField apellidoDos;

    @FXML
    private Button btnRegistrar;

    @FXML
    private Button atras;

    @FXML
    private DatePicker idFechaDefuncion;

    @FXML
    private TextField idDescripcion;

    @FXML
    private TextField idNombreArtistico;

    @FXML
    private TextField pais;

    @FXML
    void registroArtista(ActionEvent event) {
        String fechaDefuncion = null;
        String fechaNacimiento1 = null;
        if (idFechaDefuncion.getValue() != null) {
            fechaDefuncion = idFechaDefuncion.getValue().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));

        }
        if (nombre.getText().length() == 0 || apellidoUno.getText().length() == 0 || apellidoDos.getText().length() == 0 || idNombreArtistico.getText().length() == 0) {
            JOptionPane.showMessageDialog(null, "Datos incompletos intente de nuevo");
        } else {
            if (fechaNacimiento.getValue() != null) {
                fechaNacimiento1 = fechaNacimiento.getValue().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));

                controlador.registrarArtista(nombre.getText(), apellidoUno.getText(), apellidoDos.getText(), fechaNacimiento1,
                        pais.getText(), idDescripcion.getText(), idNombreArtistico.getText(), fechaDefuncion);
            }

        }
    }

    @FXML
    void retroceder(ActionEvent event) {
        toScene.toScene("MenuUsuarioAdmin.fxml", event);
    }


}

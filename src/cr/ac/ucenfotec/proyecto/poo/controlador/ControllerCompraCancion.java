package cr.ac.ucenfotec.proyecto.poo.controlador;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;

import javax.swing.*;
import java.net.URL;
import java.util.ResourceBundle;

public class ControllerCompraCancion implements Initializable {
    Controlador controlador = new Controlador();
    ToScene toScene = new ToScene();
    @FXML
    private Label mensajeBienvenida;


    @FXML
    private Button btnAtras;

    @FXML
    private Button btnComprar;

    @FXML
    private ComboBox SeleccionCancion;

    @FXML
    private Button btnListar;

    @FXML
    void comprarCancion(ActionEvent event) {
        String nombreUsuario = JOptionPane.showInputDialog(null, "Digite su nombre usuario");
        controlador.agregarCancionColeccion(nombreUsuario, SeleccionCancion.getSelectionModel().getSelectedItem().toString());

    }

    @FXML
    void atras(ActionEvent event) {
        toScene.toScene("MenuUsuario.fxml", event);
    }

    @FXML
    void miLista(ActionEvent event) {
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        for (int i = 0; i < controlador.listarNombreCanciones().length; i++) {
            SeleccionCancion.getItems().addAll(controlador.listarNombreCanciones()[i]);
        }
    }
}

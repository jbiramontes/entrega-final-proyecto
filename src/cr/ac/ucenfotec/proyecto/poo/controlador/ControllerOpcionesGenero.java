package cr.ac.ucenfotec.proyecto.poo.controlador;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;


import java.net.URL;
import java.util.ResourceBundle;

public class ControllerOpcionesGenero implements Initializable {
    Controlador controlador = new Controlador();
    ToScene toScene = new ToScene();
    @FXML
    private TextField idNombreGenero;

    @FXML
    private TextField IdRegistroNombreGenero;

    @FXML
    private TextField idRegistroDescripcionGenero;

    @FXML
    private Button btnRegistrar;

    @FXML
    private Button atras;
    @FXML
    private ComboBox SeleccionGenero;

    @FXML
    private Button btnModificar;

    @FXML
    private TextField idDescripcionGenero;

    @FXML
    private Button btnRegistrar11;

    @FXML
    void SelectGenero(ActionEvent event) {

    }

    @FXML
    void eliminarGenero(ActionEvent event) {
        String descripcion = "";
        controlador.eliminargenero(SeleccionGenero.getSelectionModel().getSelectedItem().toString(), descripcion);
    }

    @FXML
    void modificarGenero(ActionEvent event) {

        controlador.modificargenero(SeleccionGenero.getSelectionModel().getSelectedItem().toString(),idNombreGenero.getText(), idDescripcionGenero.getText());
    }

    @FXML
    void registroGenero(ActionEvent event) {
        controlador.registrargenero(IdRegistroNombreGenero.getText(), idRegistroDescripcionGenero.getText());

    }

    @FXML
    void retroceder(ActionEvent event) {
        toScene.toScene("MenuUsuarioAdmin.fxml", event);
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        for (int i = 0; i < controlador.listarGenerosMenu().length; i++) {
            SeleccionGenero.getItems().addAll(controlador.listarGenerosMenu()[i]);
        }


    }
}

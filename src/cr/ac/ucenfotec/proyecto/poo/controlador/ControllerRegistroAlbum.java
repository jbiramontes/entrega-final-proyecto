package cr.ac.ucenfotec.proyecto.poo.controlador;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;

import java.time.format.DateTimeFormatter;

public class ControllerRegistroAlbum {

    Controlador controlador = new Controlador();
    ToScene toScene = new ToScene();

    @FXML
    private DatePicker fechaLanzamiento;

    @FXML
    private Button btnEliminar;

    @FXML
    private TextField IdNombreAlbum;

    @FXML
    private Button btnRegistrar;

    @FXML
    private Button atras1;

    @FXML
    void eliminarAlbum(ActionEvent event) {
        controlador.eliminarAlbum(IdNombreAlbum.getText());
    }

    @FXML
    void registroAlbum(ActionEvent event) {
        String fecha = fechaLanzamiento.getValue().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        controlador.crearAlbum(IdNombreAlbum.getText(), fecha);
    }

    @FXML
    void retroceder(ActionEvent event) {
        toScene.toScene("MenuUsuario.fxml", event);
    }

}

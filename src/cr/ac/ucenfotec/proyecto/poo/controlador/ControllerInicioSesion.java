package cr.ac.ucenfotec.proyecto.poo.controlador;

import cr.ac.ucenfotec.proyecto.poo.bl.logica.Gestor;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;

public class ControllerInicioSesion {
    Controlador controlador = new Controlador();
    Gestor gestor = new Gestor();

    @FXML
    private Button btnRegistro;

    @FXML
    private Button inicioSesion;

    @FXML
    private Button Salir;

    @FXML
    private TextField nombreUsuario;

    @FXML
    private PasswordField contrasena;

    @FXML
    private Button btnIngresar;

    @FXML
    private Label mensajeError;

    @FXML
    void Salir(ActionEvent event) {
        gestor.salirMenus(Salir);
    }


    public void inicioSesion(ActionEvent event) {
        String nombre = nombreUsuario.getText();
        String password = contrasena.getText();

        if (controlador.inicioSesion(nombre, password) == 1) {
            new ToScene().toScene("MenuUsuario.fxml", event);
        }
        if (controlador.inicioSesion(nombre, password) == 0) {
            new ToScene().toScene("MenuUsuarioAdmin.fxml", event);


        } else {
            mensajeError.setText("Datos invalidos");

        }

    }

    @FXML
    void registroUsuario(ActionEvent event) {
        new ToScene().toScene("RegistroUsuario.fxml", event);

    }


}

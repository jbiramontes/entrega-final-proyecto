package cr.ac.ucenfotec.proyecto.poo.controlador;

import cr.ac.ucenfotec.proyecto.poo.bl.logica.Gestor;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

import javax.swing.*;
import java.util.List;

public class ControllerMenuUsuario {
    Gestor gestor = new Gestor();
    Controlador controlador = new Controlador();
    ToScene toScene = new ToScene();
    ToScene toScene2 = new ToScene();
    ToScene toScene3 = new ToScene();
    ToScene toScene4 = new ToScene();
    @FXML
    private Button btnCompraCancion;
    @FXML
    private Button btnCrearAlbum;
    @FXML
    private Label mensajeBienvenida;
    @FXML
    private Button btnMostarListas;
    @FXML
    private Button registroCancion;
    @FXML
    private Button salir;
    @FXML
    private Button btnListarAlbumes;
    @FXML
    private Button btnColeccion;

    @FXML
    void registrarCancion(ActionEvent event) {
        toScene.toScene("RegistroCancion.fxml", event);
    }

    @FXML
    void salir(ActionEvent event) {
        controlador.Salir();
    }

    @FXML
    void crearAlbum(ActionEvent event) {
       toScene4.toScene("RegistroAlbum.fxml",event);
    }

    @FXML
    void crearLista(ActionEvent event) {
        toScene2.toScene("MenuOpcionesListaReproduccion.fxml", event);
        //controlador.crearListaReproduccion();
    }

    @FXML
    void mostrarlistasReproduccion(ActionEvent event) {

        controlador.listar(1, null);

    }

    @FXML
    void listarColeccion(ActionEvent event) {
        String nombreUsuario = JOptionPane.showInputDialog(null, "Digite su nombre usuario");
        controlador.listar(5, nombreUsuario);
    }

    @FXML
    void compraCancion(ActionEvent event) {
        toScene3.toScene("MenuCompraCancion.fxml", event);
    }

    @FXML
    void listarAlbumes(ActionEvent event) {
        new ToScene().toScene("MenuOpcionesAlbum.fxml", event);
    }

}

package cr.ac.ucenfotec.proyecto.poo.controlador;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

public class ControllerMenuAdmin {
    Controlador controlador = new Controlador();
    ToScene toScene = new ToScene();
    ToScene toScene2 = new ToScene();
    ToScene toScene3 = new ToScene();
    ToScene toScene4 = new ToScene();
    @FXML
    private Label mensajeBienvenida;

    @FXML
    private Button btnListarCanciones;

    @FXML
    private Button registroCancion;

    @FXML
    private Button btnCompositores;

    @FXML
    private Button salir;

    @FXML
    private Button btnArtistas;

    @FXML
    private Button btnGeneros;

    @FXML
    private Button btnArtistasListar;

    @FXML
    void irArtistas(ActionEvent event) {
        toScene2.toScene("RegistroArtista.fxml", event);
    }


    @FXML
    void irCompositores(ActionEvent event) {
        toScene.toScene("RegistroCompositor.fxml", event);
    }

    @FXML
    void irGeneros(ActionEvent event) {

        toScene4.toScene("MenuGeneros.fxml", event);
    }

    @FXML
    void registrarCancion(ActionEvent event) {
        toScene3.toScene("RegistroCancionAdmin.fxml", event);
    }

    @FXML
    void listarCanciones(ActionEvent event) {
        controlador.listar(4, null);
    }

    @FXML
    void salir(ActionEvent event) {
        controlador.Salir();
    }

    @FXML
    void listarArtistas(ActionEvent event) {
        controlador.listar(2, null);
    }
}

package cr.ac.ucenfotec.proyecto.poo.controlador;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class ControllerBuscarAlbum {
    Controlador controlador = new Controlador();
    @FXML
    private Label mensajeBienvenida;
    @FXML
    private Button btnAtras;
    @FXML
    private Button btnBuscarAlbum;

    @FXML
    private Button salir;

    @FXML
    private Button btnListarAlbumes;

    @FXML
    private TextField nombreAlbum;

    @FXML
    void buscarAlbum(ActionEvent event) {


        controlador.listar(10, nombreAlbum.getText());

    }

    @FXML
    void listarAlbum(ActionEvent event) {
        controlador.listar(9, null);
    }

    @FXML
    void salir(ActionEvent event) {
        controlador.Salir();
    }


    @FXML
    void Atras(ActionEvent event) {
        new ToScene().toScene("MenuUsuario.fxml", event);
    }


}

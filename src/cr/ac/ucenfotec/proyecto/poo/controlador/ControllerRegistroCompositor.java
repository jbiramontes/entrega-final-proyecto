package cr.ac.ucenfotec.proyecto.poo.controlador;


import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;

import javax.swing.*;
import java.time.format.DateTimeFormatter;


public class ControllerRegistroCompositor {
    ToScene toScene = new ToScene();
    Controlador controlador = new Controlador();
    @FXML
    private TextField edad;

    @FXML
    private TextField nombre;

    @FXML
    private TextField apellidoUno;

    @FXML
    private TextField apellidoDos;

    @FXML
    private Button btnRegistrar;

    @FXML
    private Button atras;

    @FXML
    private TextField pais;

    @FXML
    private DatePicker fechaNacimiento;
    @FXML
    private Button btnListar;

    @FXML
    void listarCompositores(ActionEvent event) {
        controlador.listar(3, null);
    }

    @FXML
    void registroCompositor(ActionEvent event) {

        if (nombre.getText().length() != 0 || apellidoUno.getText().length() != 0 || pais.getText().length() != 0) {
            if (fechaNacimiento.getValue() != null) {
                String fechaNacimiento2 = fechaNacimiento.getValue().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
                controlador.registrarCompositor(nombre.getText(), apellidoUno.getText(), apellidoDos.getText(),
                        fechaNacimiento2, pais.getText());
            }
        } else {
            JOptionPane.showMessageDialog(null, "Datos incompletos");

        }
    }

    @FXML
    void retroceder(ActionEvent event) {
        toScene.toScene("MenuUsuarioAdmin.fxml", event);
    }
}

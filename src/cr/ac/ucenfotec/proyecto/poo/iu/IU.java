package cr.ac.ucenfotec.proyecto.poo.iu;


import cr.ac.ucenfotec.proyecto.poo.bl.entidades.Usuario;

import java.io.PrintStream;
import java.util.Scanner;

public class IU {
    private Scanner input = new Scanner(System.in).useDelimiter("\n");
    private PrintStream output = new PrintStream(System.out);

    public void menuPrincipal() {
        output.println("Bienvenido a su app de musica digital");
        output.println("1. Ingresar a la aplicacion");
        output.println("2. Registrarse");
        output.println("0. Salir");


    }

    public void menuUsuario(Usuario nuevo) {
        output.println("Bienvenido " + nuevo.getNombreUsuario() + " a su biblioteca de musica");
        output.println("Favor digite el numero de la opcion que desea realizar");
        output.println("1. Reproducir cancion");
        output.println("2. Registrar  cancion");
        output.println("3. Crear un album");
        output.println("4. Comprar");
        output.println("5. Crear lista de reproduccion");
        output.println("6. Listar los album");
        output.println("7. Mis listas de reproduccion ");
        output.println("0. Salir");
    }

    public void menuInicioUsuario() {
        output.println("Bienvenido a su biblioteca de musica");
        output.println("Favor digite el numero de la opcion que desea realizar");
        output.println("1. Registrar cancion");
        output.println("2. Crear lista de reproduccion");
        output.println("3. Crear un album");
        output.println("4. Listar artistas ");
        output.println("5. Listar los album");
        output.println("6. Listar compositores");
        output.println("7. Listar canciones");
        output.println("8. Listar generos");
        output.println("9. Mis listas de reproduccion");
        output.println("0. Salir");
    }


    public void menuAdministradorApp(Usuario nuevo) {
        output.println("Bienvenido " + nuevo.getNombreUsuario());
        output.println("¿Que deseas realizar el dia de hoy?");
        output.println("1. Registrarse como usuario a la aplicacion");
        output.println("2. Registrar un artista");
        output.println("3. Registrar un album");
        output.println("4. Registrar un compositor");
        output.println("5. Registrar una Cancion");
        output.println("6. Registrar genero");
        output.println("7. Crear lista de reproduccion");
        output.println("8. Listar artistas registrados");
        output.println("9. Listar los album");
        output.println("10. Listar compositores");
        output.println("11. Listar canciones");
        output.println("12. Listar generos");
        output.println("13. Listar listas de reproduccion");
        output.println("14. Listar usuarios registrados a la aplicacion");
        output.println("0. Salir");
    }


    public int leerOpcionMenu() {
        try {
            return input.nextInt();
        } catch (Exception e) {
            System.out.println("Usted ha ingresado un dato incorrecto");
        }
        return 0;
    }

    public void mensajes(String mensaje) {
        output.println(mensaje);
    }

    public String leerTexto() {
        try {
            return input.next();

        } catch (Exception e) {
            System.out.println("Hubo un error con el tipo de dato ingresado");
        }
        return null;
    }

}//Fin de la clase IU

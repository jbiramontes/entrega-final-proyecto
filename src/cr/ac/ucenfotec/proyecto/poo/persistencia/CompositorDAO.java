package cr.ac.ucenfotec.proyecto.poo.persistencia;

import cr.ac.ucenfotec.proyecto.poo.bl.entidades.Compositor;
import cr.ac.ucenfotec.proyecto.poo.bl.entidades.Usuario;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class CompositorDAO {
    Connection cnx;
    private final String TEMPLATE_INSERTAR = "insert into tcompositor (nombre,apellidoUno,apellidoDos,edad,pais,fechaNacimiento)" + "values (?,?,?,?,?,?)";


    private PreparedStatement comandoInsertar;
    private final String TEMPLATE_CONSULTA = "select * from tcompositor";
    private PreparedStatement comandoConsulta;


    public CompositorDAO(Connection cnx) {
        this.cnx = cnx;
        try {
            this.comandoConsulta = cnx.prepareStatement(TEMPLATE_CONSULTA);
            this.comandoInsertar = cnx.prepareStatement(TEMPLATE_INSERTAR);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void save(Compositor nuevoCompositor) throws SQLException {
        LocalDate fecha = nuevoCompositor.getFechaNacimiento();
        Date fechaSql = Date.valueOf(fecha);
        if (this.comandoInsertar != null) {
            this.comandoInsertar.setString(1, nuevoCompositor.getNombre());
            this.comandoInsertar.setString(2, nuevoCompositor.getApellidoUno());
            this.comandoInsertar.setString(3, nuevoCompositor.getApellidoDos());
            this.comandoInsertar.setInt(4, nuevoCompositor.getEdad());
            this.comandoInsertar.setString(5, nuevoCompositor.getPais());
            this.comandoInsertar.setDate(6, fechaSql);
            this.comandoInsertar.execute();
        }


    }

    public List<Compositor> obtenerCompositores() {
        ArrayList<Compositor> listaCompositores = new ArrayList<>();
        try {
            ResultSet resultSet = this.comandoConsulta.executeQuery();
            while (resultSet.next()) {
                Compositor nuevoCompositor = new Compositor();
                nuevoCompositor.setNombre(resultSet.getString("nombre"));
                nuevoCompositor.setApellidoUno(resultSet.getString("apellidoUno"));
                nuevoCompositor.setApellidoDos(resultSet.getString("apellidoDos"));
                nuevoCompositor.setEdad(resultSet.getInt("edad"));
                nuevoCompositor.setPais(resultSet.getString("pais"));
                nuevoCompositor.setFechaNacimiento(resultSet.getDate("fechaNacimiento").toLocalDate());

                listaCompositores.add(nuevoCompositor);

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return listaCompositores;
    }

}


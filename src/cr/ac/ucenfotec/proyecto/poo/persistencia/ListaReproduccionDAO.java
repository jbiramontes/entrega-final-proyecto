package cr.ac.ucenfotec.proyecto.poo.persistencia;

import cr.ac.ucenfotec.proyecto.poo.PropertiesHandler;
import cr.ac.ucenfotec.proyecto.poo.bl.entidades.Cancion;
import cr.ac.ucenfotec.proyecto.poo.bl.entidades.ListaReproduccion;


import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class ListaReproduccionDAO {
    Connection cnx;
    private final String TEMPLATE_INSERTAR_LISTA = "insert into tlistareproduccion (nombreLista,fechaCreacion,idUsuario)" +
            "values (?,?,?)";
    private final String TEMPLATE_INSERTAR_CANCION = "insert into tcancionesxlista (nombreCancion,nombreLista)" +
            "values (?,?)";


    private PreparedStatement comandoInsertarCancion;
    private PreparedStatement comandoInsertarLista;
    private final String TEMPLATE_CONSULTA2 = "select * from tcancionesxlista";
    private final String TEMPLATE_CONSULTA = "select * from tlistareproduccion";
    private PreparedStatement comandoConsulta;
    private PreparedStatement comandoConsulta2;

    public ListaReproduccionDAO(Connection cnx) {
        this.cnx = cnx;
        try {
            this.comandoConsulta2 = cnx.prepareStatement(TEMPLATE_CONSULTA2);
            this.comandoInsertarCancion = cnx.prepareStatement(TEMPLATE_INSERTAR_CANCION);
            this.comandoConsulta = cnx.prepareStatement(TEMPLATE_CONSULTA);
            this.comandoInsertarLista = cnx.prepareStatement(TEMPLATE_INSERTAR_LISTA);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void save(ListaReproduccion nuevaLista) throws SQLException {
        LocalDate fecha = nuevaLista.getFechaCreacion();
        Date fechaSql = Date.valueOf(fecha);
        if (this.comandoInsertarLista != null) {
            this.comandoInsertarLista.setString(1, nuevaLista.getNombreLista());
            this.comandoInsertarLista.setDate(2, fechaSql);
            this.comandoInsertarLista.setString(3, nuevaLista.getNombreUsuario());


            this.comandoInsertarLista.execute();
        }


    }

    public void saveCancion(String nombreCancion, String nombreLista) throws SQLException {
        if (this.comandoInsertarCancion != null) {
            this.comandoInsertarCancion.setString(1, nombreLista);
            this.comandoInsertarCancion.setString(2, nombreCancion);


            this.comandoInsertarCancion.execute();
        }


    }

    public List<ListaReproduccion> obtenerListaReproduccion() {

        ArrayList<ListaReproduccion> listas = new ArrayList<>();
        try {
            ResultSet resultSet = this.comandoConsulta.executeQuery();
            ResultSet resultSet1 = this.comandoConsulta2.executeQuery();
            while (resultSet.next()) {
                ListaReproduccion nuevaLista = new ListaReproduccion();
                nuevaLista.setNombreLista(resultSet.getString("nombreLista"));
                ArrayList<Cancion> listaCancionesxLista = new ArrayList<>();
                while (resultSet1.next()) {
                    Cancion cancion = new Cancion();
                    String nombre = (resultSet1.getString("nombreLista"));
                    if (nuevaLista.getNombreLista().contentEquals(nombre)) {
                        cancion.setNombreCancion(resultSet1.getString("nombreCancion"));
                        listaCancionesxLista.add(cancion);
                    }
                }
                nuevaLista.setCancionesLista(listaCancionesxLista);
                nuevaLista.setFechaCreacion(resultSet.getDate("fechaCreacion").toLocalDate());
                nuevaLista.setNombreUsuario(resultSet.getString("idUsuario"));
                listas.add(nuevaLista);

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return listas;
    }

}


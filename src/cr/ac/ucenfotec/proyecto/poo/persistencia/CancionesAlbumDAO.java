package cr.ac.ucenfotec.proyecto.poo.persistencia;

import cr.ac.ucenfotec.proyecto.poo.bl.entidades.Album;
import cr.ac.ucenfotec.proyecto.poo.bl.entidades.Cancion;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CancionesAlbumDAO {

    Connection cnx;
    private final String TEMPLATE_INSERTAR = "insert into tcancionesalbum (nombreCancion,nombreAlbum)" + "values (?,?)";

    private PreparedStatement comandoInsertar;
    private String TEMPLATE_CONSULTA = "select * from tcancionesalbum";

    //private final String TEMPLATE_CONSULTA2 = "select nombreCancion from tcancionesalbum";
    private PreparedStatement comandoConsulta;
    private PreparedStatement comandoConsultaNombre;
    String TEMPLATE_CONSULTANOMBRE = "select * from tcancionesalbum where nombreAlbum=";

    public CancionesAlbumDAO(Connection cnx) {
        this.cnx = cnx;
        try {
            this.comandoConsultaNombre = cnx.prepareStatement(TEMPLATE_CONSULTANOMBRE);
            this.comandoConsulta = cnx.prepareStatement(TEMPLATE_CONSULTA);
            this.comandoInsertar = cnx.prepareStatement(TEMPLATE_INSERTAR);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void save(Cancion nuevaCancion, Album album) throws SQLException {

        if (this.comandoInsertar != null) {
            this.comandoInsertar.setString(1, nuevaCancion.getNombreCancion());
            this.comandoInsertar.setString(2, album.getNombreAlbum());
            this.comandoInsertar.execute();
        }


    }

    public List<Album> listarCancionesAlbum() {
        ArrayList<Album> listaCancionesAlbum = new ArrayList<>();
        ArrayList<Cancion> cancionesAlbum = new ArrayList<>();

        try {
            ResultSet resultSet = this.comandoConsulta.executeQuery();

            while (resultSet.next()) {
                Album nuevoAlbum = new Album();
                Cancion nuevaCancion = new Cancion();
                nuevaCancion.setNombreCancion(resultSet.getString("nombreCancion"));
                cancionesAlbum.add(nuevaCancion);
                nuevoAlbum.setNombreAlbum(resultSet.getString("nombreAlbum"));

                nuevoAlbum.setCancionesAlbum(cancionesAlbum);

                listaCancionesAlbum.add(nuevoAlbum);

            }


        } catch (Exception e) {
            e.printStackTrace();
        }
        return listaCancionesAlbum;
    }

    public List<Album> listarAlbumesNombre(String nombre) {
        ArrayList<Album> listaCancionesAlbum = new ArrayList<>();
        String nombre1 = "'" + nombre + "'";

        try {
            ResultSet resultSet = this.comandoConsulta.executeQuery();
            while (resultSet.next()) {
                if (nombre1.contains(resultSet.getString("nombreAlbum"))) {
                    Album nuevoAlbum = new Album();
                    Cancion nuevaCancion = new Cancion();
                    nuevoAlbum.setNombreAlbum(resultSet.getString("nombreAlbum"));
                    nuevaCancion.setNombreCancion(resultSet.getString("nombreCancion"));
                    ArrayList<Cancion> cancionesAlbum = new ArrayList<>();
                    cancionesAlbum.add(nuevaCancion);
                    nuevoAlbum.setCancionesAlbum(cancionesAlbum);
                    listaCancionesAlbum.add(nuevoAlbum);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listaCancionesAlbum;
    }
}

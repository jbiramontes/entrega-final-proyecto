package cr.ac.ucenfotec.proyecto.poo.persistencia;

import cr.ac.ucenfotec.proyecto.poo.PropertiesHandler;
import cr.ac.ucenfotec.proyecto.poo.bl.entidades.Album;
import cr.ac.ucenfotec.proyecto.poo.bl.entidades.Cancion;
import cr.ac.ucenfotec.proyecto.poo.bl.entidades.Genero;
import cr.ac.ucenfotec.proyecto.poo.bl.entidades.Usuario;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class AlbumDAO {

    Connection cnx;

    private final String TEMPLATE_INSERTAR = "insert into talbum (nombre,fechaLanzamiento,imagen)" + "values (?,?,?)";


    private PreparedStatement comandoInsertar;
    private final String TEMPLATE_CONSULTA = "select * from talbum";
    private PreparedStatement comandoConsulta;
    private final String TEMPLATE_BORRAR = "delete from talbum where nombre=?";
    private PreparedStatement comandoBorrar;

    public AlbumDAO(Connection cnx) {
        this.cnx = cnx;
        try {
            this.comandoBorrar=cnx.prepareStatement(TEMPLATE_BORRAR);
            this.comandoConsulta = cnx.prepareStatement(TEMPLATE_CONSULTA);
            this.comandoInsertar = cnx.prepareStatement(TEMPLATE_INSERTAR);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void save(Album nuevoAlbum) throws SQLException {
        LocalDate fecha = nuevoAlbum.getFechaLanzamiento();
        Date fechaSql = Date.valueOf(fecha);
        if (this.comandoInsertar != null) {
            this.comandoInsertar.setString(1, nuevoAlbum.getNombreAlbum());
            this.comandoInsertar.setDate(2, fechaSql);
            this.comandoInsertar.setString(3, nuevoAlbum.getImagen());

            this.comandoInsertar.execute();
        }


    }

    public List<Album>obtenerAlbum() {
        ArrayList<Album> listaAlbumes = new ArrayList<>();
        try {
            ResultSet resultSet = this.comandoConsulta.executeQuery();
            while (resultSet.next()) {
                Album nuevoAlbum = new Album();
                nuevoAlbum.setNombreAlbum(resultSet.getString("nombre"));
                nuevoAlbum.setFechaLanzamiento(resultSet.getDate("fechaLanzamiento").toLocalDate());
                nuevoAlbum.setImagen(resultSet.getString("imagen"));

                listaAlbumes.add(nuevoAlbum);

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return listaAlbumes;
    }
    public void eliminarAlbum(Album album) throws SQLException {

        if (this.comandoBorrar != null) {
            this.comandoBorrar.setString(1, album.getNombreAlbum());
            this.comandoBorrar.execute();
        }
    }
}

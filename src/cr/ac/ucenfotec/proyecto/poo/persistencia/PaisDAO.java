package cr.ac.ucenfotec.proyecto.poo.persistencia;



import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class PaisDAO {
    Connection cnx;
    private final String TEMPLATE_MODIFICAR = "update tpais set nombrePais=? where nombrePais=?" ;
    private PreparedStatement comandoModificar;
    private final String TEMPLATE_INSERTAR = "insert into tpais (nombrePais)" + "values (?)";
    private PreparedStatement comandoInsertar;
    private final String TEMPLATE_CONSULTA = "select * from tpais";
    private PreparedStatement comandoConsulta;
    private final String TEMPLATE_BORRAR = "delete from tpais where nombrePais=?";
    private PreparedStatement comandoBorrar;

    public PaisDAO(Connection cnx) {
        this.cnx = cnx;
        try {
            this.comandoBorrar=cnx.prepareStatement(TEMPLATE_BORRAR);
            this.comandoModificar=cnx.prepareStatement(TEMPLATE_MODIFICAR);
            this.comandoConsulta = cnx.prepareStatement(TEMPLATE_CONSULTA);
            this.comandoInsertar = cnx.prepareStatement(TEMPLATE_INSERTAR);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void save(String nombre) throws SQLException {

        if (this.comandoInsertar != null) {
            this.comandoInsertar.setString(1, nombre);

            this.comandoInsertar.execute();
        }
    }

    public List<String> obtenerPaises() {
        ArrayList<String> listaPaises = new ArrayList<>();
        try {
            ResultSet resultSet = this.comandoConsulta.executeQuery();
            while (resultSet.next()) {

                listaPaises.add(resultSet.getString("nombrePais"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listaPaises;
    }
    public void modificar(String nombre,String nombreNuevo) throws SQLException {

        if (this.comandoModificar != null) {
            this.comandoModificar.setString(1, nombreNuevo);
            this.comandoModificar.setString(2,nombre);

            this.comandoModificar.execute();
        }
    }
    public void eliminar(String pais) throws SQLException {

        if (this.comandoBorrar != null) {
            this.comandoBorrar.setString(1, pais);
            this.comandoBorrar.execute();
        }
    }
}


package cr.ac.ucenfotec.proyecto.poo.persistencia;

import cr.ac.ucenfotec.proyecto.poo.PropertiesHandler;
import cr.ac.ucenfotec.proyecto.poo.bl.entidades.Persona;
import cr.ac.ucenfotec.proyecto.poo.bl.entidades.Usuario;

import java.io.IOException;
import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class UsuarioDao {
    Connection cnx;
    private final String TEMPLATE_INSERTAR = "insert into tusuario (nombre,apellido_uno,apellido_dos,identificacion,edad,nombreUsuario,contrasena,pais,fechaNacimiento,avatar,correoElectronico,tipoUsuario)" + "values (?,?,?,?,?,?,?,?,?,?,?,?)";


    private PreparedStatement comandoInsertar;
    private final String TEMPLATE_CONSULTA = "select * from tusuario";
    private PreparedStatement comandoConsulta;


    public UsuarioDao(Connection cnx) {
        this.cnx = cnx;
        try {
            this.comandoConsulta = cnx.prepareStatement(TEMPLATE_CONSULTA);
            this.comandoInsertar = cnx.prepareStatement(TEMPLATE_INSERTAR);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void save(Usuario nuevoUsuario) throws SQLException {
       try{ LocalDate fecha = nuevoUsuario.getFechaNacimiento();
        Date fechaSql = Date.valueOf(fecha);
        if (this.comandoInsertar != null) {
            this.comandoInsertar.setString(1, nuevoUsuario.getNombre());
            this.comandoInsertar.setString(2, nuevoUsuario.getApellidoUno());
            this.comandoInsertar.setString(3, nuevoUsuario.getApellidoDos());
            this.comandoInsertar.setString(4, nuevoUsuario.getIdentificacion());
            this.comandoInsertar.setInt(5, nuevoUsuario.getEdad());
            this.comandoInsertar.setString(6, nuevoUsuario.getNombreUsuario());
            this.comandoInsertar.setString(7, nuevoUsuario.getContrasena());
            this.comandoInsertar.setString(8, nuevoUsuario.getPais());
            this.comandoInsertar.setDate(9, fechaSql);
            this.comandoInsertar.setString(10, nuevoUsuario.getImagen());
            this.comandoInsertar.setString(11, nuevoUsuario.getCorreoUsuario());
            this.comandoInsertar.setInt(12, nuevoUsuario.getTipoUsuario());
            this.comandoInsertar.execute();
        }} catch (SQLException throwables) {
           //throwables.printStackTrace();
       }


    }

    public List<Usuario> obtenerUsuarios() {
        ArrayList<Usuario> listaUsuarios = new ArrayList<>();
        try {
            ResultSet resultSet = this.comandoConsulta.executeQuery();
            while (resultSet.next()) {
                Usuario nuevoUsuario = new Usuario();
                nuevoUsuario.setNombre(resultSet.getString("nombre"));
                nuevoUsuario.setApellidoUno(resultSet.getString("apellido_uno"));
                nuevoUsuario.setApellidoDos(resultSet.getString("apellido_dos"));
                nuevoUsuario.setIdentificacion(resultSet.getString("identificacion"));
                nuevoUsuario.setEdad(resultSet.getInt("edad"));
                nuevoUsuario.setNombreUsuario(resultSet.getString("nombreUsuario"));
                nuevoUsuario.setContrasena(resultSet.getString("contrasena"));
                nuevoUsuario.setPais(resultSet.getString("pais"));
                nuevoUsuario.setFechaNacimiento(resultSet.getDate("fechaNacimiento").toLocalDate());
                nuevoUsuario.setImagen(resultSet.getString("avatar"));
                nuevoUsuario.setCorreoUsuario(resultSet.getString("correoElectronico"));
                nuevoUsuario.setTipoUsuario(resultSet.getInt("tipoUsuario"));

                listaUsuarios.add(nuevoUsuario);

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return listaUsuarios;
    }
    public List<Usuario> obtenerCredenciales() {
        ArrayList<Usuario> listaUsuarios = new ArrayList<>();
        try {
            ResultSet resultSet = this.comandoConsulta.executeQuery();
            while (resultSet.next()) {
                Usuario nuevoUsuario = new Usuario();
                nuevoUsuario.setNombreUsuario(resultSet.getString("nombreUsuario"));
                nuevoUsuario.setContrasena(resultSet.getString("contrasena"));
                nuevoUsuario.setTipoUsuario(resultSet.getInt("tipoUsuario"));
                listaUsuarios.add(nuevoUsuario);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return listaUsuarios;
    }

}
  /*  public void save(Usuario usuario) throws SQLException {
        Statement stmt = cnx.createStatement();
        StringBuilder buildSentence = new StringBuilder("insert into tusuario (nombre,apellido_uno,apellido_dos,identificacion,\" +\n" +
                "                    \"edad,nombreUsuario,contrasena,pais,fechaNacimiento,avatar,correoElectronico)");
        buildSentence.append(" values ('");
        buildSentence.append(usuario.getPersonaUsuario().getNombre());
        buildSentence.append("','");
        buildSentence.append(usuario.getPersonaUsuario().getApellidoUno());
        buildSentence.append("',");
        buildSentence.append(usuario.getPersonaUsuario().getApellidoDos());
        buildSentence.append(",'");
        buildSentence.append(usuario.getIdentificacion());
        buildSentence.append("')");
        buildSentence.append(usuario.getPersonaUsuario().getEdad());
        buildSentence.append("')");
        buildSentence.append(usuario.getNombreUsuario());
        buildSentence.append("')");
        buildSentence.append(usuario.getContrasena());
        buildSentence.append("')");
        buildSentence.append(usuario.getPersonaUsuario().getPais());
        buildSentence.append("')");
        buildSentence.append(usuario.getPersonaUsuario().getFechaNacimiento());
        buildSentence.append("')");
        buildSentence.append(usuario.getImagen());
        buildSentence.append("')");
        buildSentence.append(usuario.getCorreoUsuario());
        buildSentence.append("')");
        System.out.println(buildSentence.toString());
        stmt.execute(buildSentence.toString());
    }
*/

   /* public List<Usuario> findAll() throws SQLException {
        ArrayList<Usuario> listOfResults = new ArrayList<>();
        Statement stmt = cnx.createStatement();
        ResultSet result = stmt.executeQuery("select * from tusuario");
        while (result.next()) {
            Usuario nuevoUsuario = new Usuario();
            nuevoUsuario.setNombreUsuario(result.getString("nombreUsuario"));
            nuevoUsuario.setContrasena(result.getString("contrasena"));

            nuevoUsuario.setIdentificacion("identificacion");
            nuevoUsuario.setImagen(result.getString("avatar"));
            nuevoUsuario.setCorreoUsuario(result.getString("correoElectronico"));
            nuevoUsuario.setTipoUsuario(result.getString("tipoUsuario"));
            listOfResults.add(nuevoUsuario);
            System.out.println(nuevoUsuario.toString() + "prueba");
        }
        return listOfResults;*/



      /*  try {
            System.out.println("Espere un momento");
//Load the Driver
            propertiesHandler.loadProperties();
            String driver = propertiesHandler.getDriver();
            Class.forName(driver).newInstance();
             //System.out.println("LOADED DRIVER ---> " + driver);
// Create the "url"
// assume database server is running on the localhost
            String url = propertiesHandler.getCnxStr();
// Connect to the database represented by url
// with username root and password admin
            Connection con =
                    DriverManager.getConnection(url,propertiesHandler.getUsrname(), propertiesHandler.getPassword());
            //System.out.println("CONNECTED TO ---> " + url);
// Use the Connection to create a Statement object
            Statement stmt = con.createStatement();
//Insert data

            //System.out.println("INSERT INFO TO ---> " + url);
            boolean result = stmt.execute("insert into tusuario (nombre,apellido_uno,apellido_dos,identificacion," +
                    "edad,nombreUsuario,contrasena,pais,fechaNacimiento,avatar,correoElectronico)" +
                    "values ('" + ((Usuario) A).getPersonaUsuario().getNombre() + "','" + ((Usuario) A).getPersonaUsuario().getApellidoUno() + "'," +
                    "'" + ((Usuario) A).getPersonaUsuario().getApellidoDos() + "','" + ((Usuario) A).getIdentificacion() + "'," +
                    "'" + ((Usuario) A).getPersonaUsuario().getEdad() + "','" + ((Usuario) A).getNombreUsuario() + "'," +
                    "'" + ((Usuario) A).getContrasena() + "','" + ((Usuario) A).getPersonaUsuario().getPais() + "'," +
                    "'" + ((Usuario) A).getPersonaUsuario().getFechaNacimiento() + "','" + ((Usuario) A).getImagen() + "','"+((Usuario)A).getCorreoUsuario()+"')");

            stmt.close();
            con.close();
            System.out.println("Registro exitoso");
        } catch (
                SQLException e) {
         //   System.out.println("NO se pudo efectuar el registro"/*+e.toString()*///);
        /*} catch (ClassNotFoundException e) {
            System.out.println("2" + e.toString());
        } catch (InstantiationException e) {
            System.out.println("3" + e.toString());
        } catch (IllegalAccessException e) {
            System.out.println("4" + e.toString());
        }

    }*/

    /*
    public ArrayList<String> extraerCredencialUsuario(){
ArrayList<String> credenciales=new ArrayList<>();
        try {

            System.out.println("START");
//Load the Driver
            String driver ="com.mysql.cj.jdbc.Driver";
            Class.forName(driver).newInstance();
            System.out.println("LOADED DRIVER ---> " + driver);
// Create the "url"
// assume database server is running on the localhost
            String url = "jdbc:mysql://localhost:3306/bdappbaulrecuerdos";
// Connect to the database represented by url
// with username root and password admin
            Connection con =
                    DriverManager.getConnection (url, "root", "root");
            System.out.println("CONNECTED TO ---> "+ url);
// Use the Connection to create a Statement object
            Statement stmt = con.createStatement ();

// Execute query using Statement, receive the ResultSet
            String qry ="SELECT * FROM tusuario";
            ResultSet resultadoConsulta = stmt.executeQuery(qry);
            //System.out.println("EXECUTED QUERY ---> " + qry);
            // Print the results, row by row
            //System.out.println("\nPROCESSING RESULTS:\n");
            while (resultadoConsulta.next()) {
                credenciales.add(resultadoConsulta.getString("nombre"));
                credenciales.add(resultadoConsulta.getString("correoElectronico"));
                credenciales.add(resultadoConsulta.getString("correoElectronico"));

            }
            resultadoConsulta.close();
            stmt.close();
            con.close();




        return credenciales;
        } catch (
                SQLException e) { System.out.println("No se encuentran datos"/*+e.toString()*//*)*/;

      /*  } catch (ClassNotFoundException e) { System.out.println("2"+e.toString());
        } catch (InstantiationException e) { System.out.println("3"+e.toString());
        } catch (IllegalAccessException e) { System.out.println("4"+e.toString());

        }
        return null;
    }*/



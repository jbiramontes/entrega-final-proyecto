package cr.ac.ucenfotec.proyecto.poo.persistencia;


import cr.ac.ucenfotec.proyecto.poo.bl.entidades.Cancion;
import cr.ac.ucenfotec.proyecto.poo.bl.entidades.Catalogo;
import cr.ac.ucenfotec.proyecto.poo.bl.entidades.Usuario;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CatalogoDAO {

    Connection cnx;
    private final String TEMPLATE_INSERTAR = "insert into tcancionesxusuario (nombreUsuario,nombreCancion)" + "values (?,?)";

    private PreparedStatement comandoInsertar;
    private String TEMPLATE_CONSULTA = "select * from tcancionesxusuario";


    private PreparedStatement comandoConsulta;
    private PreparedStatement comandoConsultaNombre;
    String TEMPLATE_CONSULTANOMBRE = "select * from tcancionesxusuario where nombreUsuario=?";

    public CatalogoDAO(Connection cnx) {
        this.cnx = cnx;
        try {
            this.comandoConsultaNombre = cnx.prepareStatement(TEMPLATE_CONSULTANOMBRE);
            this.comandoConsulta = cnx.prepareStatement(TEMPLATE_CONSULTA);
            this.comandoInsertar = cnx.prepareStatement(TEMPLATE_INSERTAR);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void save(Cancion nuevaCancion, Usuario usuario) throws SQLException {

        if (this.comandoInsertar != null) {
            this.comandoInsertar.setString(1, usuario.getNombreUsuario());
            this.comandoInsertar.setString(2, nuevaCancion.getNombreCancion());
            this.comandoInsertar.execute();
        }


    }

    public List<Catalogo> listarCatalogos() {
        ArrayList<Catalogo> listaCatalogos = new ArrayList<>();

        try {
            ResultSet resultSet = this.comandoConsulta.executeQuery();

            while (resultSet.next()) {

                ArrayList<Cancion> cancionesCatalogo = new ArrayList<>();
                Cancion nuevaCancion = new Cancion();
                Catalogo nuevoCatalogo = new Catalogo();
                nuevaCancion.setNombreCancion(resultSet.getString("nombreCancion"));
                cancionesCatalogo.add(nuevaCancion);
                Usuario nuevoUsuario = new Usuario();
                nuevoUsuario.setNombreUsuario(resultSet.getString("nombreUsuario"));
                nuevoCatalogo.setUsuarioCatalogo(nuevoUsuario);
                nuevoCatalogo.setCancionesLista(cancionesCatalogo);

                listaCatalogos.add(nuevoCatalogo);

            }


        } catch (Exception e) {
            e.printStackTrace();
        }
        return listaCatalogos;
    }


}

package cr.ac.ucenfotec.proyecto.poo.bl.entidades;

import java.time.LocalDate;

public class Usuario extends Persona {
    private String identificacion;
    private String imagen;
    private String correoUsuario;
    private String nombreUsuario;
    private String contrasena;
    private int tipoUsuario;



    /**
     * Atributos de clase Usuario
     */
    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getCorreoUsuario() {
        return correoUsuario;
    }

    public void setCorreoUsuario(String correoUsuario) {
        this.correoUsuario = correoUsuario;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public int getTipoUsuario() {
        return tipoUsuario;
    }

    public void setTipoUsuario(int tipoUsuario) {
        this.tipoUsuario = tipoUsuario;
    }

    public Usuario() {
    }

    public Usuario(String nombre, String apellidoUno, String apellidoDos, LocalDate fechaNacimiento, int edad,
                   String pais, String identificacion, String imagen, String correoUsuario, String nombreUsuario, String contrasena, int tipoUsuario) {
        super(nombre, apellidoUno, apellidoDos, fechaNacimiento, edad, pais);
        this.identificacion = identificacion;
        this.imagen = imagen;
        this.correoUsuario = correoUsuario;
        this.nombreUsuario = nombreUsuario;
        this.contrasena = contrasena;
        this.setTipoUsuario(tipoUsuario);

    }

    @Override
    public String toString() {
        return "Usuario--" +
                "identificacion='" + identificacion + '\'' +
                ", imagen='" + imagen + '\'' +
                ", correoUsuario='" + correoUsuario + '\'' +
                ", nombreUsuario='" + nombreUsuario + '\'' +
                ", contrasena='" + contrasena + '\'' +
                ", tipoUsuario='" + tipoUsuario + '\'' +
                ", nombre='" + nombre + '\'' +
                ", apellidoUno='" + apellidoUno + '\'' +
                ", apellidoDos='" + apellidoDos + '\'' +
                ", fechaNacimiento=" + fechaNacimiento +
                ", edad=" + edad +
                ", pais='" + pais + '\'';
    }


}

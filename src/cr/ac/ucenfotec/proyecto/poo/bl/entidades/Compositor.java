package cr.ac.ucenfotec.proyecto.poo.bl.entidades;

import java.time.LocalDate;

public class Compositor extends Persona{

    public Compositor() {
    }

    public Compositor(String nombre, String apellidoUno, String apellidoDos, LocalDate fechaNacimiento, int edad, String pais) {
        super(nombre, apellidoUno, apellidoDos, fechaNacimiento, edad, pais);
    }

    @Override
    public String toString() {
        return "Compositor{" +
                "nombre='" + nombre + '\'' +
                ", apellidoUno='" + apellidoUno + '\'' +
                ", apellidoDos='" + apellidoDos + '\'' +
                ", fechaNacimiento=" + fechaNacimiento +
                ", edad=" + edad +
                ", pais='" + pais + '\'' +
                "} " + super.toString();
    }
}

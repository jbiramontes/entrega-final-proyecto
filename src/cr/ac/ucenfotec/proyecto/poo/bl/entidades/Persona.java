package cr.ac.ucenfotec.proyecto.poo.bl.entidades;

import java.time.LocalDate;
import java.util.Objects;

public abstract class Persona {
    /**
     * Atributos de clase Persona
     */
    protected String nombre;
    protected String apellidoUno;
    protected String apellidoDos;
    protected LocalDate fechaNacimiento;
    protected int edad;
    protected String pais;


    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidoUno() {
        return apellidoUno;
    }

    public void setApellidoUno(String apellidoUno) {
        this.apellidoUno = apellidoUno;
    }

    public String getApellidoDos() {
        return apellidoDos;
    }

    public void setApellidoDos(String apellidoDos) {
        this.apellidoDos = apellidoDos;
    }

    public LocalDate getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(LocalDate fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public Persona() {
    }

    public Persona(String nombre, String apellidoUno, String apellidoDos, LocalDate fechaNacimiento, int edad, String pais) {
        this.nombre = nombre;
        this.apellidoUno = apellidoUno;
        this.apellidoDos = apellidoDos;
        this.fechaNacimiento = fechaNacimiento;
        this.edad = edad;
        this.pais = pais;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Persona)) return false;
        Persona persona = (Persona) o;
        return edad == persona.edad &&
                Objects.equals(nombre, persona.nombre) &&
                Objects.equals(apellidoUno, persona.apellidoUno) &&
                Objects.equals(apellidoDos, persona.apellidoDos) &&
                Objects.equals(fechaNacimiento, persona.fechaNacimiento) &&
                Objects.equals(pais, persona.pais);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nombre, apellidoUno, apellidoDos, fechaNacimiento, edad, pais);
    }

    @Override
    public String toString() {
        return
                "nombre='" + nombre + '\'' +
                ", apellidoUno='" + apellidoUno + '\'' +
                ", apellidoDos='" + apellidoDos + '\'' +
                ", fechaNacimiento='" + fechaNacimiento + '\'' +
                ", edad='" + edad + '\'' +
                ", pais='" + pais + '\'' +
                '}';

    }
}

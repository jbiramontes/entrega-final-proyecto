package cr.ac.ucenfotec.proyecto.poo.bl.entidades;

import java.util.ArrayList;


public class Catalogo {

    private Usuario usuarioCatalogo = new Usuario();
    private ArrayList<Cancion> cancionesLista=new ArrayList<>();



    public Usuario getUsuarioCatalogo() {
        return usuarioCatalogo;
    }

    public void setUsuarioCatalogo(Usuario usuarioCatalogo) {
        this.usuarioCatalogo = usuarioCatalogo;
    }

    public ArrayList<Cancion> getCancionesLista() {
        return cancionesLista;
    }

    public void setCancionesLista(ArrayList<Cancion> cancionesLista) {
        this.cancionesLista = cancionesLista;
    }

    public Catalogo() {

    }

    public Catalogo(Usuario usuarioCatalogo, ArrayList<Cancion> cancionesLista) {
        this.usuarioCatalogo = usuarioCatalogo;
        this.cancionesLista = cancionesLista;
    }

    @Override
    public String toString() {
        return "Catalogo--" +
                "usuarioCatalogo=" + usuarioCatalogo +
                ", cancionesLista=" + cancionesLista ;
    }
}

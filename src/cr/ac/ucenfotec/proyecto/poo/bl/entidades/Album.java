package cr.ac.ucenfotec.proyecto.poo.bl.entidades;


import java.time.LocalDate;
import java.util.ArrayList;

public class Album {
    /**
     * Atributos de clase Album
     */
    private String nombreAlbum;
    private LocalDate fechaLanzamiento;
    private String imagen;
    private ArrayList<Cancion> CancionesAlbum = new ArrayList<>();


    /**
     * Metodo para obtener nombre album
     *
     * @return nombre album
     */
    public String getNombreAlbum() {
        return nombreAlbum;
    }

    /**
     * Set para nombre del album
     *
     * @param nombreAlbum nombre de album
     */
    public void setNombreAlbum(String nombreAlbum) {
        this.nombreAlbum = nombreAlbum;
    }

    /**
     * Metodo para obtener fecha de lanzamiento
     *
     * @return fecha de lanzamiento
     */
    public LocalDate getFechaLanzamiento() {
        return fechaLanzamiento;
    }

    /**
     * Set para fecha de lanzamiento
     *
     * @param fechaLanzamiento fecha de lanzamiento
     */
    public void setFechaLanzamiento(LocalDate fechaLanzamiento) {
        this.fechaLanzamiento = fechaLanzamiento;
    }

    /**
     * Metodo para obtener imagen de album
     *
     * @return imagen de album
     */
    public String getImagen() {
        return imagen;
    }

    /**
     * Set para imagen de album
     *
     * @param imagen imagen
     */
    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public ArrayList<Cancion> getCancionesAlbum() {
        return CancionesAlbum;
    }

    public void setCancionesAlbum(ArrayList<Cancion> cancionesAlbum) {
        CancionesAlbum = cancionesAlbum;
    }

    /**
     * Constructor sin parametros
     */
    public Album() {
    }

    /**
     * Constructor con parametros
     *
     * @param nombreAlbum      nombre album
     * @param fechaLanzamiento fecha lanzamiento
     * @param imagen           imagen
     */
    public Album(String nombreAlbum, LocalDate fechaLanzamiento, String imagen, ArrayList canciones) {
        this.nombreAlbum = nombreAlbum;
        this.fechaLanzamiento = fechaLanzamiento;
        this.imagen = imagen;
        this.CancionesAlbum = canciones;
    }

    /**
     * Metodo para extraer informacion de objeto album
     *
     * @return informacion del album
     */
    @Override
    public String toString() {
        return "Album--" +
                "nombreAlbum='" + nombreAlbum + '\'' +
                ", fechaLanzamiento='" + fechaLanzamiento + '\'' +
                ", imagen='" + imagen + '\'' +
                ", canciones=" + CancionesAlbum +
                ' ';
    }


}

package cr.ac.ucenfotec.proyecto.poo.bl.entidades;

public class Genero {
    /**
     * Atributos de clase Genero
     */
    private String nombre;
    private String descripcionGenero;


    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcionGenero() {
        return descripcionGenero;
    }

    public void setDescripcionGenero(String descripcionGenero) {
        this.descripcionGenero = descripcionGenero;
    }

    public Genero() {
    }

    public Genero(String nombre, String descripcionGenero) {
        this.nombre = nombre;
        this.descripcionGenero = descripcionGenero;
    }

    @Override
    public String toString() {
        return "Genero--" +
                "nombre='" + nombre + '\'' +
                ", descripcionGenero='" + descripcionGenero + '\'' +
                ' ';
    }
}

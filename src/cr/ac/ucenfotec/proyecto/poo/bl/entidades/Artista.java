package cr.ac.ucenfotec.proyecto.poo.bl.entidades;

import java.time.LocalDate;
import java.util.Objects;

public class Artista extends Persona {
    private String descripcion;
    private String nombreArtistico;
    private LocalDate fechaDefuncion;


    /**
     * Atributos de clase Artista
     */
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getNombreArtistico() {
        return nombreArtistico;
    }

    public void setNombreArtistico(String nombreArtistico) {
        this.nombreArtistico = nombreArtistico;
    }

    public LocalDate getFechaDefuncion() {
        return fechaDefuncion;
    }

    public void setFechaDefuncion(LocalDate fechaDefuncion) {
        this.fechaDefuncion = fechaDefuncion;
    }

    public Artista() {
    }

    public Artista(String nombre, String apellidoUno, String apellidoDos, LocalDate fechaNacimiento, int edad, String pais,
                   String descripcion, String nombreArtistico, LocalDate fechaDefuncion) {
        super(nombre, apellidoUno, apellidoDos, fechaNacimiento, edad, pais);
        this.descripcion = descripcion;
        this.nombreArtistico = nombreArtistico;
        this.fechaDefuncion = fechaDefuncion;
    }

    @Override
    public String toString() {
        return "Artista--" +
                "descripcion='" + descripcion + '\'' +
                ", nombreArtistico='" + nombreArtistico + '\'' +
                ", fechaDefuncion=" + fechaDefuncion +
                ", nombre='" + nombre + '\'' +
                ", apellidoUno='" + apellidoUno + '\'' +
                ", apellidoDos='" + apellidoDos + '\'' +
                ", edad=" + edad +
                ", pais='" + pais + '\'';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Artista)) return false;
        if (!super.equals(o)) return false;
        Artista artista = (Artista) o;
        return Objects.equals(descripcion, artista.descripcion) &&
                Objects.equals(nombreArtistico, artista.nombreArtistico) &&
                Objects.equals(fechaDefuncion, artista.fechaDefuncion);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), descripcion, nombreArtistico, fechaDefuncion);
    }
}
